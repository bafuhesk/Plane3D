//	2014年11月26日
//	Encode	USC-2 little Endian
//	format	Dos/windows

#include "C_InfoLayer.h"

USING_NS_CC;

C_InfoLayer *C_InfoLayer::create(std::string const &filename) {
    C_InfoLayer *layer = new C_InfoLayer();
    if (layer && layer->init(filename)) {
        layer->autorelease();
    }
    else
        CC_SAFE_DELETE(layer);
    return layer;
}

bool C_InfoLayer::init(std::string const &filename) {
    Layer::init();

	Vec2 winCenter = winSize / 2;

    Sprite *sprite = Sprite::create(filename);
    sprite->setPosition(winCenter);
    addChild(sprite);
    sprite->runAction(Sequence::create(
            ScaleTo::create(0.2f, 0.2f),
            ScaleTo::create(0.2f, 0.5f),
            ScaleTo::create(0.2f, 0.8f),
            NULL));
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    listener->onTouchBegan = CC_CALLBACK_2(C_InfoLayer::onTouchBegan, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    return true;
}


bool C_InfoLayer::onTouchBegan(Touch *touch, Event *event) {
    this->removeFromParent();
    return true;
}
