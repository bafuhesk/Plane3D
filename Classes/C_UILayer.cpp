//	Encode	USC-2 little Endian
//	format	Dos/windows
// Created by daniel on 14/12/1.
//

#include "C_UILayer.h"
#define winSize Director::getInstance()->getVisibleSize()
#define winCenter Vec2(winSize.width/2, winSize.height/2)
bool C_UILayer::init() {
    Layer::init();
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("loadingAndHP.plist" , "loadingAndHP.png");
    //血条背景框
	Sprite* spriteHPempty = Sprite::createWithSpriteFrameName("hp_empty.png");
	//Sprite* spriteHPempty = Sprite::createWithTexture(Director::getInstance()->getTextureCache()->addImage("hp_empty.png"));
    spriteHPempty->setPosition(spriteHPempty->getBoundingBox().size.width/2,
            winSize.height-spriteHPempty->getBoundingBox().size.height/2);
    addChild(spriteHPempty,1);
    //血条 进度条
    Sprite* spriteHP = Sprite::createWithSpriteFrameName("hp.png");
    v_timer = ProgressTimer::create(spriteHP);
    v_timer->setType(ProgressTimer::Type::BAR);
    v_timer->setMidpoint(Vec2(0,0));
    v_timer->setPercentage(100);
    v_timer->setBarChangeRate(Vec2(0,1));
    v_timer->setPosition(spriteHPempty->getPositionX()-10, spriteHPempty->getPositionY()+24);
    addChild(v_timer,20);
    //战绩 背景框
    Sprite* scoreBG = Sprite::createWithSpriteFrameName("right_top_ui.png");
    scoreBG->setPosition(winSize.width-scoreBG->getBoundingBox().size.width/2,
            winSize.height-scoreBG->getBoundingBox().size.height/2);
    addChild(scoreBG,20);
    //战绩
    v_score = LabelAtlas::create("0","score_right_top.png",23,28,'0');
    //v_score->setScale(0.85f);
    v_score->setPosition(winSize.width - 0.8f * scoreBG->getBoundingBox().size.width,
            winSize.height-scoreBG->getBoundingBox().size.height/6);
    addChild(v_score, 50);


    return true;
}
//重置 血条 战绩
void C_UILayer::reset() {
    setHP(100);
    setScore(0);
}
//血条 设置(百分制)
void C_UILayer::setHP(int i) {
    v_timer->setPercentage(i);
}
//成绩 设置
void C_UILayer::setScore(unsigned int i) {
    v_score->setString(__String::createWithFormat("%5d",i)->getCString());
}
