//	2014年11月29日 14:07:19
//	Encode	USC-2 little Endian
//	format	Dos/windows

#include "C_3DPlane.h"
#include "C_Particle.h"
#include "C_Bullet.h"
#include "C_BulletManager.h"
#include "C_FoeManager.h"

//飞机爆炸效果
void C_Plane::boom()
{
	//爆炸烟雾效果
	//ParticleSystemQuad * b = ParticleSystemQuad::create("toonSmoke.plist");
	//this->getParent()->addChild(b);
	//b->setPosition(getPosition());
	//爆炸碎片粒子效果
	if (getParent()) {	//飞机在别的节点上的时候
		ParticleSystem * d = C_Particle::getInstance()->add_particle("debris.plist");
		this->getParent()->addChild(d);
		//addChild(d);
		d->setPosition(getPosition());
	}
	//d->setScale(5.0f);
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//

//开场飞机的初始化
bool C_StartPlane::init()
{
	if (!C_Plane::init()) {
		return false;
	}
	initWithFile("playerv002.obj");
	setTexture("playerv002_256.png");
	setRotation3D(Vec3(-25.0f , 150.0f , 10.0f));
	setAnchorPoint(Vec2(0.5f,.5f));
	setScale(35);
	auto dl = C_DrawLine3D::create();
	dl->setlineColor(Vec3(255 , 0 , 0));
	dl->setlineWidth(50);
	dl->drawWithSprite(this , Mat4());
	return true;
}

//开场飞机的自动旋转运动
void C_StartPlane::automatic_rotate()
{
	float scale = getScale();
	setScale(scale*0.1);	//设置缩放为原来的0.1倍
	ScaleBy* st = ScaleBy::create(2.0f , scale);	//创建缩放动作
	ScaleTo* st2 = ScaleTo::create(2.0f , 0.1f * scale);

	setPosition(Vec2::ZERO);	//设置位置到零点
	//创建移动到动作，从零点移动到中心点
	Size winsize = Director::getInstance()->getWinSize();
	Size off = Size(50.0f , 150.0f);
	MoveTo* mt = MoveTo::create(2.0f , winsize-off);
	MoveTo* mt2 = MoveTo::create(2.0f , Vec2::ZERO+off);

	Sequence* seq1 = Sequence::create(st , st2 , nullptr);
	Sequence* seq2 = Sequence::create(mt , mt2 , nullptr);
	Spawn* spa = Spawn::createWithTwoActions( seq1, seq2);
	this->runAction(RepeatForever::create(spa));

	/**
	RotateBy* rt = RotateBy::create(2.0f , Vec3(5.0f , 20.0f , -20.0f));
	//RotateBy* rrt = RotateBy::create(1.5f , Vec3(.0f , 50.0f , -16.0f));
	RotateBy* rrt = rt->reverse();

	MoveBy * mb = MoveBy::create(2.0f , Vec2(0.0f , 60.0f));
	MoveBy *rmb = mb->reverse();
	Sequence *seq1 = Sequence::create(rt , rrt , nullptr);
	Sequence *seq2 = Sequence::create(mb , rmb,nullptr);
	Spawn* spa = Spawn::createWithTwoActions(seq1 , seq2);
	this->runAction(RepeatForever::create(spa));
	*/
}

//################################################################################//

//敌人飞机的创建，传入创建类型
C_FoePlane* C_FoePlane::create(TYPE type)
{
	C_FoePlane * ret = new C_FoePlane;
	if (ret) {
		ret->v_type = type;
	}
	if (ret->init()) {
		//ret->autorelease(); //无需自动内存管理，再重复利用的时候会出错。从父节点移除，再添加
		return ret;
	}
	delete ret;
	ret = nullptr;

	return ret;
}

//敌人飞机的初始化
bool C_FoePlane::init()
{
	if (!C_Plane::init()) {
		return false;
	}

	//初始化飞机实体
	switch (v_type) {
	case C_FoePlane::PaoHui:
		initWithFile("dijiyuanv001.obj");	//炮灰敌机的模型
		setTexture("dijiyuanv001.png");
		setScale(5.0f);
		break;
	case C_FoePlane::ZhengChang:
		initWithFile("diji1_v002.obj");
		setTexture("diji02_v002_128.png");
		setScale(5.0f);
		break;
	case C_FoePlane::Boss:
		initWithFile("boss.obj");	//boss敌机的模型
		setTexture("boss.png");		//boss敌机的贴图纹理
		{	//添加炮台
			Sprite3D *bossCannon1 = Sprite3D::create("bossCannon.obj" , "boss.png");
			Sprite3D *bossCannon2 = Sprite3D::create("bossCannon.obj" , "boss.png");
			addChild(bossCannon1);
			addChild(bossCannon2);
			//Size CS = Size(4.0f , 8.0f);
			//bossCannon1->setScale(3.0f);
			//bossCannon2->setScale(3.0f);
			/*bossCannon1->setLocalZOrder(getZOrder() + 10);
			bossCannon2->setLocalZOrder(getZOrder() + 10);*/
			bossCannon1->setPosition3D(Vec3(1.5f , .0f , 3.7f));
			bossCannon2->setPosition3D(Vec3(-1.5f , .0f , 3.7f));
			
		}
		setScale(12.0f);
		break;
	default:
		break;
	}
	
	setRotation3D(Vec3(-90.0f,180.0f,180.0f));
	setAnchorPoint(Vec2(0.5f , .5f));
	return true;
}

//敌机的死亡
void C_FoePlane::die()
{
	boom();	//播放爆炸动画
	//removeFromParentAndCleanup(true);
}

//敌机的自动飞行
void C_FoePlane::automatic_fly()
{
	//先滑行到屏幕中间偏上位置
	Size WinSize = Director::getInstance()->getWinSize();
	Size End = Size( WinSize.width * (rand_0_1()*0.5+0.25), WinSize.height * 0.75f);
	ccBezierConfig c;
	c.controlPoint_1=this->getPosition();
	c.controlPoint_2 = Vec2(c.controlPoint_1.x , End.height);
	c.endPosition = End;
	BezierTo* bt = BezierTo::create(1.0f , c);

	//如果不是大Boss飞机，就向屏幕下方飞行，否则屏幕中间绕圈
	ActionInterval * act = nullptr;
	switch (v_type) {
	case C_FoePlane::PaoHui:
		//炮灰向飞机下部飞行
		act = MoveBy::create(10.0f , Vec2(0 , -WinSize.height));
		break;
	case C_FoePlane::ZhengChang:
		//正常飞机在这边瞎转，打酱油
		//act = Sequence::createWithTwoActions(MoveTo::create(5.0f , Vec2(rand_0_1()*(WinSize.width - 100.0f) + 50.0f , getPosition().y)) ,
		//									 CallFunc::create(this , callfunc_selector(C_FoePlane::shoot)));
		act = MoveTo::create(5.0f , Vec2(rand_0_1()*(WinSize.width - 100.0f) + 50.0f , WinSize.height*0.75f));
		break;
	case C_FoePlane::Boss:
		act = Sequence::create(
			MoveTo::create(1.0f , Vec2(WinSize.width *0.5f , WinSize.height *0.75f)) ,
			CallFunc::create(this , callfunc_selector(C_FoePlane::shoot)),
			nullptr	//必须要，否则变参函数没有结束标志出错
			);
		break;
	default:
		break;
	}
	
	runAction(RepeatForever::create(Sequence::createWithTwoActions(bt , act)));
}

//敌机发射子弹
void C_FoePlane::shoot()
{
	auto * hd1 = C_FoeBUlletManager::getInstance()->get_bullet();
	auto * hd2 = C_FoeBUlletManager::getInstance()->get_bullet();
	
	getParent()->addChild(hd1);
	getParent()->addChild(hd2);
	
	switch (v_type) {
	case C_FoePlane::PaoHui:
		break;
	case C_FoePlane::ZhengChang:
		hd1->setPosition(getPosition() + Vec2(20.0f , 0.0f));
		hd2->setPosition(getPosition() + Vec2(-20.0f , 0.0f));
		break;
	case C_FoePlane::Boss:
		hd1->setPosition(getPosition() + Vec2(18.0f , -44.0f));
		hd2->setPosition(getPosition() + Vec2(-18.0f ,-44.0f));
		break;
	default:
		break;
	}

	
	Point to = Point(100.0f , 100.0f);
	
	//子弹子弹飞行，子弹的方向子弹自身控制
	hd1->fly_to(to);
	hd2->fly_to(to);
}
//################################################################################//

//玩家飞机的初始化
bool C_PlayerPlane::init()
{
	if (!C_Plane::init()) {
		return false;
	}
	//初始化飞机实体
	initWithFile("playerv002.obj");
	setTexture("playerv002_256.png");
	setScale(8.0f);
	//设置飞机初始角度
	setRotation3D(Vec3(90.0f , 0.0f , 0.0f));
	//添加引擎排气粒子效果
	ParticleSystem * ps = C_Particle::getInstance()->add_particle("engine.plist");
	addChild(ps);
	ps->setScale(0.07f);	//设置粒子缩放
	ps->setTotalParticles(5);	//设置粒子总数
	//ps->setSpeed(10.0f);
	ps->setEmissionRate(50);	//设置粒子发射速度
	ps->setPositionType(ParticleSystem::PositionType::GROUPED);	//设置位置类型 grouped v.组合；集合；聚集；成群
	ps->setRotation3D(Vec3(270.0f , 0.0f , 0.0f));	//设置粒子角度
	Vec3 mz(.0f , .0f , 3.0f);	//相对飞机中心偏移
	ps->setPosition3D(getPosition3D() + mz);	//设置粒子位置
	
	//添加拖尾效果
	Texture2D * t = Director::getInstance()->getTextureCache()->addImage("streak.png");
	//创建并且初始化一个拖动渐隐效果包含参数fade:渐隐时间（以秒为单位），minSeg:最小的片段（渐隐片段的大小）
	//stroke:渐隐条带的宽度，color：顶点颜色值, path:纹理文件名 
	MotionStreak* mt = MotionStreak::create(5.0f , 10.0f , 5.0f , Color3B::YELLOW , t);
	mt->setScale(100.0f);
	addChild(mt);
	mt->setPosition3D(ps->getPosition3D());
	//注册触摸事件
	reg_touch();

	return true;
}


//子弹发射函数，玩家飞机专属
void C_PlayerPlane::shoot()
{
	if (getParent()==nullptr) {
		return;	//没有加到其他节点上就直接略过
	}
	C_Bullet* ld[5] = { nullptr };
	Point winsize = Director::getInstance()->getWinSize();
	int off = (int)getPosition().x;
	off /= winsize.x / 6;	//控制自动最左边最右边
	winsize.x /= 10.0f;	//分作10份，子弹朝向中间位置飞
	//winsize.y *= 2.0f;	//以2倍远处为目标
	
	for (int i = 0; i < 5; ++i) {
		ld[i]= C_PlayerBUlletManager::getInstance()->get_bullet();
		if (ld[i]) {
			getParent()->addChild(ld[i]);
			ld[i]->setPosition(getPosition());
			ld[i]->fly_to(Point(winsize.x*(i+off) , winsize.y));
		}
	}
}

//玩家飞机发射导弹
void C_PlayerPlane::shoot(Node* target, float dist)
{
	//获取一个导弹
	C_DaoDan* bt = (C_DaoDan*)C_PlayerBUlletManager::getInstance()->get_DaoDan();
	//添加到飞机的父节点上
	this->getParent()->addChild(bt);
	//设置位置
	bt->setPosition(getPosition());
	//开启子弹自动飞行
	bt->fly_to(target , dist);
}

//################################################################################//
//玩家飞机死亡
void C_PlayerPlane::die()
{
	boom();
	removeFromParentAndCleanup(true);
}

//玩家飞机的触摸事件
void C_PlayerPlane::reg_touch()
{
	//设置触摸事件，飞机左右飞
	auto tev = EventListenerTouchOneByOne::create();
	tev->setEnabled(true);

	//设置触摸开始调用
	tev->onTouchBegan = [this](Touch* t, Event*)->bool{
		//this->setPosition(t->getLocation());
		//从敌机管理类中随机获取一个目标作为导弹的目标
		shoot(C_FoeManager::getInstance()->get_one_run_foe());	//发射导弹
		shoot();	//发射蓝弹
		return true; 
	};

	//设置触摸移动调用
	tev->onTouchMoved = [&](Touch* t , Event*){
		//Vec2 start = t->getStartLocation();	//起点始终都是点击下去时候的点，手指移动后也是
		//Vec2 end = t->getLocation();	//当前点就是触摸位置的点
		//cocos2d::log("start x =%f y = %f\t end x = %f y = %f" , start.x , start.y , end.x , end.y);
		//cocos2d::log("end - start x =%f y = %f" , end.x - start.x , end.y- start.y );

		//向左的时候
		if (t->getDelta().x <0) {
			this->setRotation3D(Vec3(90.0f , -50.0f , 0.0f));
		}
		else {
			this->setRotation3D(Vec3(90.0f , 50.0f , 0.0f));
		}
		this->setPosition(getPosition()+t->getDelta());	//获取位置增量
		
	};

	//设置触摸结束调用
	tev->onTouchEnded = [this](Touch* t , Event*){
		this->setRotation3D(Vec3(90.0f , 0.0f , 0.0f));
	};

	//将触摸事件加入分发器中
	_eventDispatcher->addEventListenerWithSceneGraphPriority(tev , this);
}

//玩家飞机的入场自翻转
void C_PlayerPlane::enter_flip()
{
	//FlipY3D * fp3 = FlipY3D::create(1.0f);	//返回NULL
	RotateBy * rt = RotateBy::create(0.7f , Vec3(.0f , 720.0f , .0f));
	this->runAction(rt);
}