//	2014年11月29日 14:07:19
//	Encode	USC-2 little Endian
//	format	Dos/windows


#ifndef __EFFECT3D_H__
#define __EFFECT3D_H__

#include "cocos2d.h"
USING_NS_CC;

class C_Effect3D : public Ref
{
public:
	
protected:
	C_Effect3D(): _glProgramState(nullptr) {}//构造函数
	virtual ~C_Effect3D()//析构函数
	{
		CC_SAFE_RELEASE(_glProgramState);//清除变量内存占用
	}
protected:
	GLProgramState* _glProgramState;//定义opengl变量
};

class C_DrawLine3D : public C_Effect3D//3d效果边线类
{
public:
	static C_DrawLine3D* create();//创建3d特效边线

	void setlineColor(const Vec3& color);//设置3d特效边线颜色

	void setlineWidth(float width);//设置3d特效边线宽度

	void drawWithSprite(Sprite3D* sprite , const Mat4 &transform);//渲染带特效的精灵

protected:

	C_DrawLine3D();//构造函数
	virtual ~C_DrawLine3D();//析构函数

	bool init();//初始化

	Vec3	v_lineColor;//定义3d边线颜色
	float	v_lineWidth;//定义3d边线宽度
public:
	static const std::string _vertShaderFile;//定义顶点着色器文件
	static const std::string _fragShaderFile;//定义片断着色器文件
	static const std::string _keyInGLProgramCache;//定义opengl程序缓存
	static GLProgram* getOrCreateProgram();//获取opengl程序实例
};




#endif // !__EFFECT3D_H__





