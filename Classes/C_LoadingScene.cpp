//	Encode	USC-2 little Endian
//	format	Dos/windows
// Created by daniel on 14/11/28.
//
//#include "Vector.h"
#include "C_LoadingScene.h"
#include "C_BattleLayer.h"
//
float C_LoadingScene::cocoRotation = 0;

std::string images[] {
"boss.png",
"coco.png",
"groundLevel.jpg",
"bullets.png",
"daodan_32.png",
"diji02_v002_128.png",
"dijiyuanv001.png",
"playerv002_256.png",
"streak.png",
"gameover_score_num_0.png",
"num_0.png",
"score_right_top.png",
"gameover.png"
};

Scene *C_LoadingScene::createScene() {
    Scene *scene = Scene::create();
    C_LoadingScene *layer = C_LoadingScene::create();
    scene->addChild(layer);
    return scene;
}

bool C_LoadingScene::init() {
    if (!Layer::init()) return false;
    cocoRotation = 0;
    v_currentNum = 0;
    v_totalNum = TOTAL_PIC_NUM + TOTAL_MUSIC_NUM;
    step = 0;
    init_Background();
    init_Coco();
    setProgressbar();
    Loading_Resource();

    scheduleUpdate();
    return true;
}

void C_LoadingScene::Loading_Resource(){
    Loading_Music();
    Loading_Pic();
    Loading_Particle();
}

void C_LoadingScene::Loading_Music(){
    SimpleAudioEngine *Audio = CocosDenshion::SimpleAudioEngine::getInstance();
    Audio->preloadEffect("explodeEffect.mp3");//预加载音效
    Loading_Callback(this);
    Audio->preloadEffect("hit.mp3");//预加载音效
    Loading_Callback(this);
    Audio->preloadEffect("boom2.mp3");//预加载音效
    Loading_Callback(this);
    Audio->preloadEffect("boom.mp3");//预加载音效
    Loading_Callback(this);
    Audio->preloadBackgroundMusic("Orbital Colossus_0.mp3");//预加载音效
    Loading_Callback(this);
    Audio->playBackgroundMusic("Flux2.mp3");//预加载背景音乐
    Loading_Callback(this);
}
void C_LoadingScene::Loading_Pic() {
    TextureCache *cache = Director::getInstance()->getTextureCache();
    for (int i = 0; i < TOTAL_PIC_NUM; ++i) {
        cache->addImageAsync(images[i], CC_CALLBACK_1(C_LoadingScene::Loading_Callback,this));
    }
}

void C_LoadingScene::Loading_Particle(){

}

void C_LoadingScene::Loading_Callback(Ref* ref) {
    ++v_currentNum;
    progress_thumb->setPositionX( (float)v_currentNum * step );
    v_pPercent->setString(__String::createWithFormat("%.2f",((float)v_currentNum / v_totalNum) )->getCString());
	if (v_currentNum == v_totalNum) {
		//cocos2d::log("ssssssssssssssssssssssssssssssssssssssssssssssssssssss");
		//切换到游戏战斗场景

		Scene* s = Scene::create();
		C_BattleLayer* bt = C_BattleLayer::create();
		s->addChild(bt);
		Director::getInstance()->replaceScene(s);
	}
}

//初始化背景
void C_LoadingScene::init_Background() {
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("loadingAndHP.plist", "loadingAndHP.png");
    Sprite *bg = Sprite::createWithSpriteFrameName("loading_bk.png");
    bg->setPosition(winCenter.x, winCenter.y);
    addChild(bg);
}
//初始化 旋转的3D精灵
void C_LoadingScene::init_Coco() {
    coco = Sprite3D::create("coconut.obj", "coco.png");
    coco->setPosition(winCenter);
    addChild(coco);
}
//初始化进度条
void C_LoadingScene::setProgressbar() {
    Sprite *progress_bk = Sprite::createWithSpriteFrameName("loading_progress_bk.png");
    progress_bk->setPosition(winSize.width/2,
            winSize.height - progress_bk->getBoundingBox().size.height);
    addChild(progress_bk);
    progress_thumb = Sprite::createWithSpriteFrameName("loading_progress_thumb.png");
    progress_thumb->setPosition(0, //progress_thumb->getBoundingBox().size.width/2,
            winSize.height - progress_thumb->getBoundingBox().size.height/2);
    addChild(progress_thumb);
    v_pPercent = Label::createWithBMFont("num.fnt","0%");
    v_pPercent->setPosition(winSize.width/2, progress_bk->getPositionY() - 3*v_pPercent->getBoundingBox().size.height);
    addChild(v_pPercent);
    step = (winSize.width - (progress_thumb->getBoundingBox().size.width/2)) / v_totalNum;
}

void C_LoadingScene::update(float dt) {
    Node::update(dt);
    cocoRotation += 10;
    coco->setRotation3D(Vec3(0,cocoRotation,0));
}