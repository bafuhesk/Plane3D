//	Encode	USC-2 little Endian
//	format	Dos/windows

#ifndef __C_BATTLELAYER_H__
#define __C_BATTLELAYER_H__

#include "cocos2d.h"
USING_NS_CC;

#include "C_3DPlane.h"
#include "C_Particle.h"
#include "C_SlideGround.h"
#include "C_Bullet.h"
#include "C_UILayer.h"

//战斗层,飞机，敌机，子弹的父节点
class C_BattleLayer : public cocos2d::Layer
{
public:
    
    virtual bool init();
    // a selector callback
	CREATE_FUNC(C_BattleLayer);

	void update(float dt);
protected:
	C_UILayer*	v_score_ui;
	C_PlayerPlane* v_player;
	int v_score;
	int v_player_life;	//玩家总的命数
	int v_Total_ph = 20;	//炮灰飞机的总数，产生这么多个后就不产生了
private:
};

#endif // __C_BATTLELAYER_H__
