#include "C_StartScene.h"
#include "C_InfoLayer.h"
#include "C_StartMenuLayer.h"
#include "C_3DPlane.h"
#include "C_LoadingScene.h"

Scene *C_StartScene::createScene() {
    Scene *scene = Scene::create();
    C_StartScene *layer = C_StartScene::create();
    scene->addChild(layer);
    return scene;
}

bool C_StartScene::init() {
    if(!Layer::init()) return false;
    //背景图片
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("menu_scene.plist", "menu_scene.png");
    Sprite *bg = Sprite::createWithSpriteFrameName("mainmenu_BG.png");
    bg->setPosition(winCenter);
    addChild(bg, itemsZorder::bgZorder);

    //menu层
    C_StartMenuLayer *menuLayer = C_StartMenuLayer::create();
    addChild(menuLayer,itemsZorder::infolayerZorder);

    //粒子效果
    ParticleSystemQuad* particle = ParticleSystemQuad::create("vanishingPoint.plist");
    particle->setStartColor(Color4F(255,255,255,255));
    particle->setEndColor(Color4F(60,60,60,60));
    particle->setStartSize(3.0f);
    this->addChild(particle, itemsZorder::particleZorder);
    particle->setPosition(winCenter);

    //3D飞机
	//C_ThreeDPlane* plane3d = C_ThreeDPlane::create();
	
	auto plane3d = C_StartPlane::create();
    this->addChild(plane3d);
	plane3d->setPosition(winCenter);
	plane3d->automatic_rotate();
    


	//Size sprite3dsize = plane3d->getContentSize();
    plane3d->setLocalZOrder(itemsZorder::planeZorder);

    return true;
}

