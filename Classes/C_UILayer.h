//	Encode	USC-2 little Endian
//	format	Dos/windows
//文档作者 山鹏, 时间 2014年12月01日
//战斗场景的 血条 战绩 显示.

#ifndef __C_UILayer_H_
#define __C_UILayer_H_

#include "cocos2d.h"

USING_NS_CC;
class C_UILayer :public Layer{
public:
    CREATE_FUNC(C_UILayer);
    virtual bool init();
    void reset();//重置 血条 战绩
    void setHP(int);//血条 设置(百分制)
    void setScore(unsigned int);//成绩 设置
private:
    ProgressTimer *v_timer;//血条 进度条
    LabelAtlas*v_score;//战绩
};


#endif //__C_UILayer_H_
