//文件编码格式(默认为UTF8)
//文档平台格式(默认为WINDOWS格式)
//文档作者 山鹏, 时间 2014年11月29日15:13:38
//开始菜单场景的 菜单layer

#include "C_InfoLayer.h"
#include "C_StartMenuLayer.h"
#include "C_LoadingScene.h"
bool C_StartMenuLayer::init() {
    Layer::init();
    showMenu();
    return true;
}

void C_StartMenuLayer::showMenu() {
    //license按钮
    MenuItemSprite *licenseMenu = MenuItemSprite::create(
            Sprite::createWithSpriteFrameName("license.png"),
            Sprite::createWithSpriteFrameName("license.png"),
            CC_CALLBACK_1(C_StartMenuLayer::licenseMenu_CallBack,this));
    licenseMenu->setPosition(licenseMenu->getBoundingBox().size.width/2,
            licenseMenu->getBoundingBox().size.height/2);
    licenseMenu->setScale(0.8f);
    licenseMenu->setTag(items::license);

    //credits按钮
    MenuItemSprite *creditsMenu= MenuItemSprite::create(
            Sprite::createWithSpriteFrameName("credits.png"),
            Sprite::createWithSpriteFrameName("credits.png"),
            CC_CALLBACK_1(C_StartMenuLayer::creditsMenu_CallBack,this));
    creditsMenu->setPosition( winSize.width - creditsMenu->getBoundingBox().size.width/2,
            creditsMenu->getBoundingBox().size.height/2);
    creditsMenu->setScale(0.8f);
    creditsMenu->setTag(items::credit);

    //start game按钮
    MenuItemSprite *startGameMenu = MenuItemSprite::create(
            Sprite::createWithSpriteFrameName("start_game.png"),
            Sprite::createWithSpriteFrameName("start_game.png"),
            CC_CALLBACK_1(C_StartMenuLayer::startGameMenu_CallBack,this));
    startGameMenu->setPosition(winCenter.x,
            2 * startGameMenu->getBoundingBox().size.height);
    startGameMenu->setScale(0.8f);
    startGameMenu->setTag(items::startGame);

    //guide按钮
    Sprite *guideSprite = Sprite::create("guide.png");
    Sprite *guideSprite2 = Sprite::create("guide.png");
    guideSprite2->setScale(1.2f);
    MenuItemSprite *guideMenu = MenuItemSprite::create(guideSprite, guideSprite2,
            CC_CALLBACK_1(C_StartMenuLayer::guideMenu_CallBack, this));
    guideMenu->setPosition(winCenter.x, guideMenu->getBoundingBox().size.height);
    guideMenu->setScale(0.8f);
    guideMenu->setTag(items::guide);
//    Vec2 p2= this->getChildByTag(items::guide)->getPosition();
//    log("x=%f, y=%f", p2.x, p2.y);

    Menu *menu = Menu::create(licenseMenu, creditsMenu, startGameMenu, guideMenu, nullptr);
    menu->setTag(items::menu);
    addChild(menu);
    menu->setPosition(0,0);
}

void C_StartMenuLayer::licenseMenu_CallBack(Ref *ptr) {
    CallFunc *func = CallFunc::create([this](){
        C_InfoLayer *infoLayer = C_InfoLayer::create("LICENSE_03.png");
        addChild(infoLayer);}
    );
    ((Node*)ptr)->runAction(Sequence::create(
            ScaleTo::create(0.1f, 0.5f),
            ScaleTo::create(0.1f, 0.8f),
            ScaleTo::create(0.1f, 1.0f),
            func,
            ScaleTo::create(0.1f, 0.8f),
            NULL)
    );
}

void C_StartMenuLayer::creditsMenu_CallBack(Ref *ptr) {
    CallFunc *func = CallFunc::create([this](){
        C_InfoLayer *infoLayer = C_InfoLayer::create("credits_03.png");
        addChild(infoLayer);}
    );
    ((Node*)ptr)->runAction(Sequence::create(
            ScaleTo::create(0.1f, 0.5f),
            ScaleTo::create(0.1f, 0.8f),
            ScaleTo::create(0.1f, 1.0f),
            func,
            ScaleTo::create(0.1f, 0.8f),
            NULL)
    );
}
// 加载 下一个过度场景
void C_StartMenuLayer::startGameMenu_CallBack(Ref *ptr) {
    Director::getInstance()->replaceScene(C_LoadingScene::createScene());
}

void C_StartMenuLayer::guideMenu_CallBack(Ref *ptr) {
    //屏蔽 菜单点击
    static_cast<Menu*>(this->getChildByTag(items::menu))->setEnabled(false);

    clip = ClippingNode::create();//创建裁剪节点
    clip->setInverted(true);//设置底板可见 
    clip->setAlphaThreshold(0);//设置alpha为0  
    this->addChild(clip,100);

    LayerColor *colorBG = LayerColor::create(Color4B(0,50,50,120),//一个黑色带透明（看起了是灰色）的底板 
            this->getContentSize().width, this->getContentSize().height);
    clip->addChild(colorBG);

    front = Node::create();//创建模版
    front->addChild(Sprite::createWithSpriteFrameName("license.png"));//模版形状

    //提示 文字
    lbl = Label::createWithSystemFont("", "Times", 40);
    //要突出显示位置
    clipPoint = (this->getChildByTag(items::menu))->getChildByTag(items::license)->getPosition();
    front->setPosition(clipPoint);
	//lbl->setString("版权信息");
	lbl->setString("BanQuanXinXi");
    lbl->setPosition(clipPoint.x, clipPoint.y + 2*lbl->getBoundingBox().size.height);
    addChild(lbl,100);
    clip->setStencil(front);//模版生效

    //向导 触摸循环开始
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(C_StartMenuLayer::guideTouchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(C_StartMenuLayer::guideTouchEnded, this);
    listener->setSwallowTouches(true);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}
bool C_StartMenuLayer::guideTouchBegan(Touch*, Event*){
    return true;
}
void C_StartMenuLayer::guideTouchEnded(Touch*, Event*){
    static int count = 1;
    ++count;

    switch(count){
        case 1:   //突出 版权信息
//            clipPoint = (this->getChildByTag(items::menu))->getChildByTag(items::license)->getPosition();
//            front->setPosition(clipPoint);
//            lbl->setString("版权信息");
//            lbl->setPosition(clipPoint.x, clipPoint.y + 2*lbl->getBoundingBox().size.height);
//            clip->setStencil(front);
            break;
        case 2://突出 开始游戏"
            clipPoint = (this->getChildByTag(items::menu))->getChildByTag(items::startGame)->getPosition();
            front->setPosition(clipPoint);
			lbl->setString("KaiShiYouXi");
			//lbl->setString("开始游戏");
            lbl->setPosition(clipPoint.x, clipPoint.y + 2*lbl->getBoundingBox().size.height);
            clip->setStencil(front);
            break;
        case 3://突出 开发者
            clipPoint = (this->getChildByTag(items::menu))->getChildByTag(items::credit)->getPosition();
            front->setPosition(clipPoint);
			lbl->setString("KaiFaZhe");
			//lbl->setString("开发者");
            lbl->setPosition(clipPoint.x, clipPoint.y + 2*lbl->getBoundingBox().size.height);
            clip->setStencil(front);
            break;
        case 4: //重置 遮罩层
            count = 0;
            lbl->removeFromParentAndCleanup(true);
            front->removeFromParentAndCleanup(true);
            clip->removeFromParentAndCleanup(true);
            _eventDispatcher->removeEventListenersForTarget(this, false);
//            _eventDispatcher->removeEventListener(listener); //相同
            //开启菜单点击
            static_cast<Menu*>(this->getChildByTag(items::menu))->setEnabled(true);
            return;
        default:
            count = 0;
            break;
    }
}