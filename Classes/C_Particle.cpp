//	2014年11月29日 14:07:19
//	Encode	USC-2 little Endian
//	format	Dos/windows

#include "C_Particle.h"

#if 0
bool C_ParticleStarry::initWithTotalParticles(int numberOfParticles)
{
	if (ParticleSystemQuad::initWithTotalParticles(numberOfParticles)) {
		// duration 持续时间
		_duration = DURATION_INFINITY;	//infinityn.无限；无穷；无穷远；无限远的点

		// Gravity Mode 重力模式
		setEmitterMode(Mode::GRAVITY);	//emitter n.辐射源；发射极

		// Gravity Mode: gravity	重力
		setGravity(Vec2(0 , 0));

		// Gravity Mode: speed of particles 粒子速度
		setSpeed(60);
		setSpeedVar(10);

		// Gravity Mode: radial	弧度
		//setRadialAccel(-80);	//加速度方向
		setRadialAccel(0);	//加速度方向
		setRadialAccelVar(0);	//加速度var

		// Gravity Mode: tangential 离心力
		//setTangentialAccel(80);
		setTangentialAccel(0);
		//setTangentialAccelVar(0);
		setTangentialAccelVar(100);

		// angle	角度
		//_angle = 90;
		_angle = 0;
		_angleVar = 360;

		// emitter position 发射粒子源位置
		Size winSize = Director::getInstance()->getWinSize();
		this->setPosition(Vec2(winSize.width / 2 , winSize.height / 2));
		//setPosVar(Vec2::ZERO);
		setPosVar(Vec2(winSize.width/4,winSize.height/3));

		// life of particles
		_life = 4;
		_lifeVar = 1;

		// size, in pixels	像素尺寸
		//_startSize = 37.0f;
		_startSize = 100.0f;
		//_startSizeVar = 10.0f;
		_startSizeVar = 50.0f;
		_endSize = START_SIZE_EQUAL_TO_END_SIZE;

		// emits per second	
		_emissionRate = _totalParticles / _life;

		// color of particles
		_startColor.r = 0.12f;
		_startColor.g = 0.25f;
		_startColor.b = 0.76f;
		_startColor.a = 1.0f;
		_startColorVar.r = 0.0f;
		_startColorVar.g = 0.0f;
		_startColorVar.b = 0.0f;
		_startColorVar.a = 0.0f;
		_endColor.r = 0.0f;
		_endColor.g = 0.0f;
		_endColor.b = 0.0f;
		_endColor.a = 1.0f;
		_endColorVar.r = 0.0f;
		_endColorVar.g = 0.0f;
		_endColorVar.b = 0.0f;
		_endColorVar.a = 0.0f;
		//_texture是成员变量，在ParticleSystem头文件中
		Texture2D* texture = _texture;
		if (texture != nullptr) {
			setTexture(texture);
		}

		// additive 添加  blend混合，融合
		this->setBlendAdditive(true);
		return true;
	}
	return false;
}

#endif


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//


C_Particle* C_Particle::object = nullptr;

C_Particle* C_Particle::getInstance()
{
	if (object==nullptr) {
		object = new C_Particle;
		//object->pars = new __Dictionary;
	}
	return object;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//

C_Particle::~C_Particle()
{
	for (auto it = v_pars.begin(); it != v_pars.end(); ++it) {
		CC_SAFE_DELETE(it->second);	//释放粒子文件所占的内存
	}
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//

//添加或获取从plistFile文件创建的粒子效果
ParticleSystem * C_Particle::add_particle(const char* plistFile)
{
	if (plistFile == nullptr) {
		return nullptr;
	}
	
	auto ref = v_pars[plistFile];

	//没有就加入
	if (ref == nullptr) {
		//vm此处只new而没有对应的delete，在析构函数中可有可无，因为单例静态对象一直存在
		ValueMap *vm = new ValueMap(FileUtils::getInstance()->getValueMapFromFile(plistFile));
		v_pars[plistFile]=vm ;
		ref = v_pars[plistFile];
	}
	ParticleSystemQuad*ret = ParticleSystemQuad::create(*ref);
	
	return (ParticleSystem *)ret;
}
