//	Encode	USC-2 little Endian
//	format	Dos/windows

#ifndef __C_Model_H__
#define __C_Model_H__

#include "cocos2d.h"
USING_NS_CC;
class C_Model :public Node
{
public:
	CREATE_FUNC(C_Model);
	bool init();

	Node* v_model;
	Size v_winsize;
};
#endif

