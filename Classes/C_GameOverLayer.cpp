//文件编码格式(默认为UTF8)
//文档平台格式(默认为WINDOWS格式)
//文档作者 山鹏, 时间 2014年12月1日
//主角 爆炸或通关后, 战绩 layer
//将 加入 排行榜

#include "C_StartScene.h"
#include "C_GameOverLayer.h"
#include "C_BattleLayer.h"

#define winSize Director::getInstance()->getVisibleSize()
#define winCenter Vec2(winSize.width/2, winSize.height/2)

C_GameOverLayer* C_GameOverLayer::create(int num){
    C_GameOverLayer *layer = new C_GameOverLayer();
    if(num < 0) //初始化 战绩
        num = 0;

    if(layer && layer->init(num)){
        layer->autorelease();
    }
    else
        CC_SAFE_DELETE(layer);
    return layer;
}
bool C_GameOverLayer::init(int num) {
    if(!Layer::init())
        return false;
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("gameover.plist","gameover.png");

    //战绩 背景
    Sprite *spriteBG = Sprite::createWithSpriteFrameName("gameover_score_bk.png");
    spriteBG->setScale(0.8f);
    spriteBG->setPosition(winCenter.x, winSize.height - spriteBG->getBoundingBox().size.height/2);
    addChild(spriteBG);

    //"SCORE"标签
    Sprite *spriteLbl = Sprite::createWithSpriteFrameName("gameover_score.png");
    spriteLbl->setScale(0.6f);
    spriteLbl->setPosition(spriteBG->getContentSize().width*0.45f, spriteBG->getContentSize().height*0.7f);
    spriteBG->addChild(spriteLbl,10);

    //战绩 分数显示
    LabelAtlas *lblScore = LabelAtlas::create(__String::createWithFormat("%d",num)->getCString(),"gameover_score_num_0.png",35,37,'0');
    lblScore->setAnchorPoint(Vec2(0.5,0.5));//lable锚点默认左下角
    lblScore->setPosition(spriteLbl->getPositionX(), spriteLbl->getPositionY()-spriteLbl->getBoundingBox().size.height);
    spriteBG->addChild(lblScore,10);

    //返回 开始菜单 场景的菜单
    Sprite *spriteBack = Sprite::createWithSpriteFrameName("gameover_backtomenu.png");
    Sprite *spriteBack2 = Sprite::createWithSpriteFrameName("gameover_backtomenu.png");
    spriteBack2->setScale(1.1f);
    MenuItemSprite *backMenu = MenuItemSprite::create(spriteBack,spriteBack2,
            CC_CALLBACK_1(C_GameOverLayer::backMenuCallBack, this));

    //重置 主战斗场景 的菜单
    Sprite *spriteAgain = Sprite::createWithSpriteFrameName("gameover_playagain.png");
    Sprite *spriteAgain2 = Sprite::createWithSpriteFrameName("gameover_playagain.png");
    spriteAgain2->setScale(1.1f);
    MenuItemSprite *playagainMenu = MenuItemSprite::create(spriteAgain, spriteAgain2,
            CC_CALLBACK_1(C_GameOverLayer::playagainMenuCallBack, this));

    //菜单位置和排列
    Menu *menu = Menu::create(backMenu, playagainMenu, nullptr );
    menu->alignItemsVerticallyWithPadding(30);
    menu->setPosition(winCenter.x, spriteBG->getPositionY() - spriteBG->getBoundingBox().size.height);
    addChild(menu,10);
	
    //屏蔽下次触摸
//    EventListenerTouchOneByOne *listener = EventListenerTouchOneByOne::create();
//    listener->setSwallowTouches(true);
//    _eventDispatcher->addEventListenerWithFixedPriority(listener, -1);
    return true;
}
// 返回 开始菜单 场景, cache 未释放
void C_GameOverLayer::backMenuCallBack(Ref *ptr) 
{
	Director::getInstance()->replaceScene(C_StartScene::createScene());
}
//重置 主战斗场景.
void C_GameOverLayer::playagainMenuCallBack(Ref *ptr) 
{
	Scene* s = Scene::create();
	C_BattleLayer* bt = C_BattleLayer::create();
	s->addChild(bt);
	Director::getInstance()->replaceScene(s);
}
