//	2014年11月29日 14:07:19
//	Encode	USC-2 little Endian
//	format	Dos/windows

#include "C_DrawLine3D.h"


const std::string C_DrawLine3D::_vertShaderFile = "Shaders3D/OutLine.vert";//设置顶点着色器文件
const std::string C_DrawLine3D::_fragShaderFile = "Shaders3D/OutLine.frag";//设置片断着色器文件
const std::string C_DrawLine3D::_keyInGLProgramCache = "Effect3DLibrary_Outline";//设置opengl缓存名字


GLProgram* C_DrawLine3D::getOrCreateProgram()
{
	auto program = GLProgramCache::getInstance()->getGLProgram(_keyInGLProgramCache);//获取opengl缓存
	if (program == nullptr)//如果为空
	{
		program = GLProgram::createWithFilenames(_vertShaderFile , _fragShaderFile);//根据顶点着色器和片断着色器生成opengl程序
		GLProgramCache::getInstance()->addGLProgram(program , _keyInGLProgramCache);//把opengl缓存添加到缓存里
	}
	return program;//返回opengl程序
}

C_DrawLine3D::C_DrawLine3D()
: v_lineWidth(1.0f)
, v_lineColor(0 , 0 , 0)
{

}

C_DrawLine3D::~C_DrawLine3D()
{

}



bool C_DrawLine3D::init()
{

	GLProgram* glprogram = C_DrawLine3D::getOrCreateProgram();//获取opengl程序
	if (nullptr == glprogram)//如果opengl程序为空
	{
		CC_SAFE_DELETE(glprogram);//清理opengl程序所占内存
		return false;//初始化失败，返回false
	}
	_glProgramState = GLProgramState::create(glprogram);//获取opengl程序的状态
	if (nullptr == _glProgramState) {
		return false;//如果opengl程序的状态为空值，返回false
	}
	_glProgramState->retain();//opengl程序的状态引用加1
	_glProgramState->setUniformVec3("LineColor" , v_lineColor);//设置用户自定义的uniforms
	_glProgramState->setUniformFloat("lineWidth" , v_lineWidth);//设置用记自定义的uniforms

	return true;
}




C_DrawLine3D* C_DrawLine3D::create()
{
	C_DrawLine3D* effect = new C_DrawLine3D();//创建3d特效边线
	if (effect && effect->init())//创建成功，返回特效
	{
		effect->autorelease();
		return effect;
	}
	else//创建失败，清理所占内存，返回空值
	{
		CC_SAFE_DELETE(effect);
		return nullptr;
	}
}



void C_DrawLine3D::setlineColor(const Vec3& color)
{
	if (v_lineColor != color)//如果边线效果跟当前使用的边线效果不一样
	{
		v_lineColor = color;///把当前使用的边线效果改为传进来的边线效果
		_glProgramState->setUniformVec3("LineColor" , v_lineColor);//把边线效果设置进opengl里
	}
}

void C_DrawLine3D::setlineWidth(float width)
{
	if (v_lineWidth != width)//如果传进来的边线效果宽度与当前正在使用的边线效果宽度不一样
	{
		v_lineWidth = width;//设置当前使用的边线效果宽度为传进来的宽度
		_glProgramState->setUniformFloat("lineWidth" , v_lineWidth);//设置opengl uniforms
	}
}


void C_DrawLine3D::drawWithSprite(Sprite3D* sprite , const Mat4 &transform)
{
	auto mesh = sprite->getMesh();//获取 3d精灵的网格模型
	int offset = 0;//偏移量设置为0
	for (auto i = 0; i < mesh->getMeshVertexAttribCount(); i++)//循环3d网格模型的顶点
	{
		auto meshvertexattrib = mesh->getMeshVertexAttribute(i);//获取第i个位置的网格模型的顶点变量

		_glProgramState->setVertexAttribPointer(s_attributeNames[meshvertexattrib.vertexAttrib] , meshvertexattrib.size , meshvertexattrib.type , GL_FALSE , mesh->getVertexSizeInBytes() , (void*)offset);//把获取的顶点变量设置进opengl程序的状态里
		offset += meshvertexattrib.attribSizeBytes;//更新偏移量
	}
	//draw
	{
		glEnable(GL_CULL_FACE);//开启剔除效果操作,seet bottom notes
		glCullFace(GL_FRONT);//剔除多边形前面
		glEnable(GL_DEPTH_TEST);//开启深度测试，什么是深度测试sett bottom notes
		GL::blendFunc(GL_SRC_ALPHA , GL_ONE_MINUS_SRC_ALPHA);//颜色混合，see notes
		Color4F color(sprite->getDisplayedColor());//获取精灵要显示的颜色
		color.a = sprite->getDisplayedOpacity() / 255.0f;//设置精灵的显示透明度

		_glProgramState->setUniformVec4("u_color" , Vec4(color.r , color.g , color.b , color.a));//把颜色设置进opengl程序的状态里

		auto mesh = sprite->getMesh();//获取精灵的网格模型
		glBindBuffer(GL_ARRAY_BUFFER , mesh->getVertexBuffer());//把精灵顶点缓存绑定到GL_ARRAY_BUFFER上
		_glProgramState->apply(transform);//设置改变
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER , mesh->getIndexBuffer());//绑定精灵网格顶点缓存到opengl元素组组缓存中
		glDrawElements((GLenum)mesh->getPrimitiveType() , mesh->getIndexCount() , (GLenum)mesh->getIndexFormat() , (GLvoid*)0);//通过opegnl 来渲染元素，glDrawElements用法,see notes
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER , 0);//缓存置空
		glBindBuffer(GL_ARRAY_BUFFER , 0);//缓存置空
		glDisable(GL_DEPTH_TEST);//关闭深度测试
		glCullFace(GL_BACK);//剔除多边线后面
		glDisable(GL_CULL_FACE);//关闭剔除效果操作
	}
}

