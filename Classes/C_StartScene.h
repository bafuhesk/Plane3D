//文件编码格式(默认为UTF8)
//文档平台格式(默认为WINDOWS格式)
//文档作者 山鹏, 时间 2014年11月26日12:33:36
//游戏主菜单
#ifndef __C_STARTSCENE_H_
#define __C_STARTSCENE_H_

#include "cocos2d.h"

USING_NS_CC;

class C_StartScene : public Layer {
public:
    static Scene *createScene();//创建场景
    CREATE_FUNC(C_StartScene);//创建主显示层
    virtual bool init(); //初始化方法，用来添加旋转飞机，开始按钮等元素
    //void update(float dt);//更新方法，用于更新旋转飞机的位置

private:
    enum itemsZorder {
        bgZorder = 0, menuZorder = 1, particleZorder = 10, planeZorder = 20, infolayerZorder = 100
    };
};


#endif //__C_STARTSCENE_H_
